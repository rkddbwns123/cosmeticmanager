package com.kyjg.cosmeticmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Cosmetic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String cosmeticType;
    @Column(nullable = false, length = 20)
    private String cosmeticName;
    @Column(nullable = false, length = 20)
    private String owner;
}
