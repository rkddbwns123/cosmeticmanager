package com.kyjg.cosmeticmanager.repository;

import com.kyjg.cosmeticmanager.entity.Cosmetic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CosmeticRepository extends JpaRepository<Cosmetic,Long> {
}
