package com.kyjg.cosmeticmanager.service;

import com.kyjg.cosmeticmanager.entity.Cosmetic;
import com.kyjg.cosmeticmanager.model.CosmeticRequest;
import com.kyjg.cosmeticmanager.repository.CosmeticRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CosmeticService {
    private final CosmeticRepository cosmeticRepository;
    public void setCosmetic(String cosmeticType, String cosmeticName, String owner) {
        Cosmetic addData = new Cosmetic();

        addData.setCosmeticType(cosmeticType);
        addData.setCosmeticName(cosmeticName);
        addData.setOwner(owner);

        cosmeticRepository.save(addData);
    }
}
