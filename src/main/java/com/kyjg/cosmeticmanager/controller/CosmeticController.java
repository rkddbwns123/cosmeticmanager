package com.kyjg.cosmeticmanager.controller;

import com.kyjg.cosmeticmanager.CosmeticManagerApplication;
import com.kyjg.cosmeticmanager.model.CosmeticRequest;
import com.kyjg.cosmeticmanager.service.CosmeticService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cosmetic")
public class CosmeticController {
    private final CosmeticService cosmeticService;
    @PostMapping("/data")
    public String setCosmetic(@RequestBody CosmeticRequest request) {
        cosmeticService.setCosmetic(request.getCosmeticType(), request.getCosmeticName(), request.getOwner());
        return "OK";
    }
}
